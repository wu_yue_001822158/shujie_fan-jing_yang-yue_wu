/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.SystemAdminRole;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.Enterprise.EnterpriseType;
import Business.Network.Network;
import Business.Organization.Organization;
import java.awt.CardLayout;
import java.util.ArrayList;
import javax.swing.JPanel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

/**
 *
 * @author wuyue
 */
public class SystemAdminWorkAreaJPanel extends javax.swing.JPanel {
    JPanel userProcessContainer;
    EcoSystem system;
    /**
     * Creates new form SystemAdminWorkAreaJPanel
     */
    public SystemAdminWorkAreaJPanel(JPanel userProcessContainer,EcoSystem system) {
        initComponents();
        this.userProcessContainer=userProcessContainer;
        this.system=system;
//        for(Network countryNetwork: system.getNetworkList()){
//            for(Network stateNetwork: countryNetwork.getNetWorkList()){
//                System.err.println(stateNetwork);
//            }
//        }
        
        populateTree();
    }

    public void populateTree(){
       DefaultTreeModel model=(DefaultTreeModel)jTree.getModel();
       
       ArrayList<Network>networkList=system.getNetworkList();
       ArrayList<Enterprise>enterpriseList;
       ArrayList<Organization>organizationList;
       
       Network network;
       Network stateNetwork = null;
       Enterprise enterprise;
       Organization organization;
       
       DefaultMutableTreeNode networks=new DefaultMutableTreeNode("Networks");
       DefaultMutableTreeNode root=(DefaultMutableTreeNode)model.getRoot();
       root.removeAllChildren();
       root.insert(networks, 0);
       
       DefaultMutableTreeNode networkNode;
       DefaultMutableTreeNode countryNode;
       DefaultMutableTreeNode stateNode = null;
       DefaultMutableTreeNode enterpriseNode;
       DefaultMutableTreeNode organizationNode;
       ArrayList<Network> countryNetworkList;
       
       for (int i = 0; i < networkList.size(); i++) {
            network = networkList.get(i);
            countryNode = new DefaultMutableTreeNode(network.getName());
            networks.insert(countryNode, i);

            enterpriseList=network.getEnterpriseDirectory().getEnterpriseList();
            for(int m=0;m<enterpriseList.size();m++){
                
                enterprise=enterpriseList.get(m);
                if(enterprise.getEnterpriseType().getValue().equals(EnterpriseType.InternationalResearchInstitute.getValue())){
                    enterpriseNode=new DefaultMutableTreeNode(enterprise.getName());
                    countryNode.insert(enterpriseNode,m);
                }
            }
                         
            for(int x=0; x<network.getNetWorkList().size();x++){
                stateNetwork=network.getNetWorkList().get(x);
                stateNode = new DefaultMutableTreeNode(stateNetwork.getName());
                countryNode.insert(stateNode,x);
                 
                enterpriseList=stateNetwork.getEnterpriseDirectory().getEnterpriseList();
                for(int n=0;n<enterpriseList.size();n++){
                    enterprise=enterpriseList.get(n);
                    if(enterprise.getEnterpriseType().getValue().equals(EnterpriseType.Restaurant.getValue())||
                      (enterprise.getEnterpriseType().getValue().equals(EnterpriseType.MedicalDepartment.getValue()))||
                       (enterprise.getEnterpriseType().getValue().equals(EnterpriseType.StateFoodSecurityAuthority.getValue()))){
                        enterpriseNode=new DefaultMutableTreeNode(enterprise.getName());
                        stateNode.insert(enterpriseNode,n);
                    }
                }
            }
            enterpriseList=network.getEnterpriseDirectory().getEnterpriseList();
            for(int j=0;j<enterpriseList.size();j++){
                enterprise=enterpriseList.get(j);
                if((enterprise.getEnterpriseType().getValue().equals(EnterpriseType.ResearchInstitute.getValue()))||
                    (enterprise.getEnterpriseType().getValue().equals(EnterpriseType.NationalFoodSecurityAuthority.getValue()))){
                    enterpriseNode=new DefaultMutableTreeNode(enterprise.getName());
                    countryNode.insert(enterpriseNode,j);
                   }
            }
          
      } 
       model.reload();
   } 
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTree = new javax.swing.JTree();
        jLabel1 = new javax.swing.JLabel();
        selectedNodeJLabel = new javax.swing.JLabel();
        manageNetworkJButton = new javax.swing.JButton();
        manageResearchInstitudeJButton = new javax.swing.JButton();
        manageEnterpriseAdminJButton = new javax.swing.JButton();
        btnmanageinternationalresearch = new javax.swing.JButton();
        btnManageEnterprise = new javax.swing.JButton();

        jTree.addTreeSelectionListener(new javax.swing.event.TreeSelectionListener() {
            public void valueChanged(javax.swing.event.TreeSelectionEvent evt) {
                jTreeValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jTree);

        jLabel1.setFont(new java.awt.Font("Toppan Bunkyu Gothic", 0, 18)); // NOI18N
        jLabel1.setText("Selected Node:");

        selectedNodeJLabel.setFont(new java.awt.Font("Toppan Bunkyu Gothic", 0, 18)); // NOI18N
        selectedNodeJLabel.setText("<View_Selected_Node>");

        manageNetworkJButton.setText("Manage Networks");
        manageNetworkJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                manageNetworkJButtonActionPerformed(evt);
            }
        });

        manageResearchInstitudeJButton.setText("Manage Research Institude ");
        manageResearchInstitudeJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                manageResearchInstitudeJButtonActionPerformed(evt);
            }
        });

        manageEnterpriseAdminJButton.setText("Manage Enterprise Admin");
        manageEnterpriseAdminJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                manageEnterpriseAdminJButtonActionPerformed(evt);
            }
        });

        btnmanageinternationalresearch.setText("Manage International Research Institude");
        btnmanageinternationalresearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnmanageinternationalresearchActionPerformed(evt);
            }
        });

        btnManageEnterprise.setText("Manage Enterprises");
        btnManageEnterprise.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnManageEnterpriseActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(98, 98, 98)
                        .addComponent(jLabel1)
                        .addGap(77, 77, 77)
                        .addComponent(selectedNodeJLabel))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(275, 275, 275)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnManageEnterprise, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(manageNetworkJButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btnmanageinternationalresearch, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(manageResearchInstitudeJButton, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(manageEnterpriseAdminJButton, javax.swing.GroupLayout.PREFERRED_SIZE, 288, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(550, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(selectedNodeJLabel)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(manageNetworkJButton)
                .addGap(26, 26, 26)
                .addComponent(btnmanageinternationalresearch)
                .addGap(18, 18, 18)
                .addComponent(manageResearchInstitudeJButton)
                .addGap(29, 29, 29)
                .addComponent(btnManageEnterprise)
                .addGap(61, 61, 61)
                .addComponent(manageEnterpriseAdminJButton)
                .addGap(128, 128, 128))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 600, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void jTreeValueChanged(javax.swing.event.TreeSelectionEvent evt) {//GEN-FIRST:event_jTreeValueChanged
        // TODO add your handling code here:
        DefaultMutableTreeNode selectedNode=(DefaultMutableTreeNode)jTree.getLastSelectedPathComponent();
        if(selectedNode!=null){
            selectedNodeJLabel .setText(selectedNode.toString());
        }

    }//GEN-LAST:event_jTreeValueChanged

    private void manageNetworkJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_manageNetworkJButtonActionPerformed
        ManageNetworkJPanel  mnjp=new ManageNetworkJPanel(userProcessContainer,system);
        userProcessContainer.add("ManageNetworkJPanel",mnjp);
        CardLayout layout=(CardLayout)userProcessContainer.getLayout();
        layout.next(userProcessContainer);

        // TODO add your handling code here:
    }//GEN-LAST:event_manageNetworkJButtonActionPerformed

    private void manageResearchInstitudeJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_manageResearchInstitudeJButtonActionPerformed

        ManageResearchInstitudeJPanel  mejp=new  ManageResearchInstitudeJPanel(userProcessContainer,system);
        userProcessContainer.add(" ManageEnterpriseJPanel",mejp);
        CardLayout layout=(CardLayout)userProcessContainer.getLayout();
        layout.next(userProcessContainer);// TODO add your handling code here:
    }//GEN-LAST:event_manageResearchInstitudeJButtonActionPerformed

    private void manageEnterpriseAdminJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_manageEnterpriseAdminJButtonActionPerformed
        ManageEnterpriseAdminJPanel  meajp=new ManageEnterpriseAdminJPanel(userProcessContainer,system);
        userProcessContainer.add("ManageEnterpriseAdminJPanel",meajp);
        CardLayout layout=(CardLayout)userProcessContainer.getLayout();
        layout.next(userProcessContainer);  // TODO add your handling code here:
    }//GEN-LAST:event_manageEnterpriseAdminJButtonActionPerformed

    private void btnmanageinternationalresearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnmanageinternationalresearchActionPerformed
        // TODO add your handling code here:
        ManageIntenationalResearchInstitudeJPanel  miri=new  ManageIntenationalResearchInstitudeJPanel(userProcessContainer,system);
        userProcessContainer.add(" ManageInternationalResearchInstitudeJPanel",miri);
        CardLayout layout=(CardLayout)userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnmanageinternationalresearchActionPerformed

    private void btnManageEnterpriseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnManageEnterpriseActionPerformed
        // TODO add your handling code here:
        ManageEnterpiseJPanel  mep=new  ManageEnterpiseJPanel(userProcessContainer,system);
        userProcessContainer.add(" ManageEnterpiseJPanel",mep);
        CardLayout layout=(CardLayout)userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_btnManageEnterpriseActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnManageEnterprise;
    private javax.swing.JButton btnmanageinternationalresearch;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTree jTree;
    private javax.swing.JButton manageEnterpriseAdminJButton;
    private javax.swing.JButton manageNetworkJButton;
    private javax.swing.JButton manageResearchInstitudeJButton;
    private javax.swing.JLabel selectedNodeJLabel;
    // End of variables declaration//GEN-END:variables
}
