/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.SystemAdminRole;

import Business.EcoSystem;
import Business.Network.Network;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author wuyue
 */
public class AddNewNetworkJPanel extends javax.swing.JPanel {
JPanel userProcessContainer;
    private EcoSystem business;
    boolean flag1;
    boolean flag2;
    boolean flag3;
    boolean flag;
    

    List<String> citiesMA = new ArrayList<>();
    List<String> citiesTX = new ArrayList<>();
    List<String> citiesMH = new ArrayList<>();
    List<String> citiesKN = new ArrayList<>();
    List<String> citiesGoa = new ArrayList<>();
    List<String> citiesTamilNadu = new ArrayList<>();
    List<String> citiesChengdu = new ArrayList<>();
    List<String> citiesBeijing = new ArrayList<>();
    List<String> citiesBahia = new ArrayList<>();
    List<String> citiesSaoPaulo = new ArrayList<>();
    List<String> citiesAbakan = new ArrayList<>();
    List<String> citiesBaley = new ArrayList<>();

    Map<String, List<String>> states_us = new HashMap<>();
    Map<String, List<String>> states_ind = new HashMap<>();
    Map<String, List<String>> states_china = new HashMap<>();
    Map<String, List<String>> states_brazil = new HashMap<>();
    Map<String, List<String>> states_russia = new HashMap<>();
    
    

    Map<String, Map<String, List<String>>> countries = new HashMap<>();

    public AddNewNetworkJPanel(JPanel userProcessContainer, EcoSystem business) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.business = business;

        citiesMA.add("Boston");
        citiesMA.add("Salem");
        citiesMA.add("Cambridge");

        citiesTX.add("Dallas");
        citiesTX.add("Arlington");

        citiesMH.add("Vashi");
        citiesMH.add("Mumbai");
        citiesMH.add("Pune");

        citiesKN.add("Mysore");
        citiesKN.add("Banglore");
        
        citiesGoa.add("Panaji");
        
        citiesTamilNadu.add("Chennai");
        citiesTamilNadu.add("Ooty");

        citiesChengdu.add("Sichuan");
        citiesBeijing.add("Beijing");
        
        citiesBahia.add("Itatim");
        citiesBahia.add("Boquira");
        
        citiesSaoPaulo.add("Andradina");
        citiesSaoPaulo.add("Araraquara");
        
        citiesAbakan.add("Yenisei");
        citiesAbakan.add("Abakan");
        
        citiesBaley.add("Krai");
        
        states_us.put("MA", citiesMA);
        states_us.put("TX", citiesTX);

        states_ind.put("MH", citiesMH);
        states_ind.put("KN", citiesKN);
        states_ind.put("GOA", citiesGoa);
        states_ind.put("TN", citiesTamilNadu);

        states_china.put("Sichuan",citiesChengdu);
        states_china.put("Beijing",citiesBeijing);
        
        states_brazil.put("Bahia",citiesBahia);
        states_brazil.put("SaoPaulo",citiesSaoPaulo);
        
        states_russia.put("Abakan", citiesAbakan);
        states_russia.put("Baley", citiesBaley);
        
        
        countries.put("USA", states_us);
        countries.put("INDIA", states_ind);
        countries.put("CHINA", states_china);
        countries.put("BRAZIL", states_brazil);
        countries.put("RUSSIA", states_russia);

        populateCountryNetwork();
    }
    /**
     * Creates new form AddNewNetworkJPanel
     */
     public void populateCountryNetwork() {
        cmbCountry.removeAllItems();
        for (Map.Entry<String, Map<String, List<String>>> entry_countries : countries.entrySet()) {
            cmbCountry.addItem(entry_countries.getKey());
        }
        
         cmbCountry.addActionListener(new ActionListener() {

           @Override
            public void actionPerformed(ActionEvent e) {
               // System.out.println("Calling Action Listener ");
                         String state = (String) cmbCountry.getSelectedItem();
       // System.out.println("get selected item : " + state);
        if (state != null) {
        populateStateNetwork();
         }
    }
        });
    }

    public void populateStateNetwork() {
        cmbState.removeAllItems();
        for (Map.Entry<String, Map<String, List<String>>> entry_countries : countries.entrySet()) {
            String selectedItem = (String) cmbCountry.getSelectedItem();
            if (entry_countries.getKey().equals(selectedItem) && selectedItem != null) {
                for (Map.Entry<String, List<String>> entry_states : entry_countries.getValue().entrySet()) {
                    cmbState.addItem(entry_states.getKey());
                }
            }
        }
        
        
                }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        btnBack = new javax.swing.JButton();
        btnReset = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        cmbCountry = new javax.swing.JComboBox();
        btnCountry = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        cmbState = new javax.swing.JComboBox();
        btnState = new javax.swing.JButton();

        jLabel1.setFont(new java.awt.Font("Toppan Bunkyu Gothic", 1, 24)); // NOI18N
        jLabel1.setText("Add New Network");

        btnBack.setText("<<Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        btnReset.setText("Reset>>");
        btnReset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResetActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Toppan Bunkyu Gothic", 0, 14)); // NOI18N
        jLabel3.setText("Select a country network -->");

        cmbCountry.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Country", " " }));
        cmbCountry.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cmbCountryMouseClicked(evt);
            }
        });
        cmbCountry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCountryActionPerformed(evt);
            }
        });

        btnCountry.setText("Add>>");
        btnCountry.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCountryActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Toppan Bunkyu Gothic", 0, 14)); // NOI18N
        jLabel4.setText("Select a state network -->");

        cmbState.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "State " }));
        cmbState.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cmbStateMouseClicked(evt);
            }
        });
        cmbState.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbStateActionPerformed(evt);
            }
        });

        btnState.setText("Add>>");
        btnState.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnStateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(37, 303, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 325, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(172, 172, 172))
            .addGroup(layout.createSequentialGroup()
                .addGap(183, 183, 183)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(cmbState, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(btnState))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(cmbCountry, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(37, 37, 37)
                                    .addComponent(btnCountry))))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnReset, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(117, 117, 117))))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(84, 84, 84)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnReset, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbCountry, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCountry))
                .addGap(65, 65, 65)
                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(64, 64, 64)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbState, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnState))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 63, Short.MAX_VALUE)
                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(47, 47, 47))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        userProcessContainer.remove(this);
        Component[] componentArray = userProcessContainer.getComponents();
        Component component = componentArray[componentArray.length - 1];
        ManageNetworkJPanel sysAdminwjp = (ManageNetworkJPanel) component;
        sysAdminwjp.populateTable();
        sysAdminwjp.populateCountryNetwork();
        sysAdminwjp.populateStateNetwork();
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResetActionPerformed

        flag1=false;
        flag2=false;
        flag3=false;
        flag=false;

    }//GEN-LAST:event_btnResetActionPerformed

    private void cmbCountryMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cmbCountryMouseClicked
       // populateStateNetwork();
    }//GEN-LAST:event_cmbCountryMouseClicked

    private void btnCountryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCountryActionPerformed

        String country = (String) cmbCountry.getSelectedItem();
        if (country == null) {
            JOptionPane.showMessageDialog(null, "Please select a Country and try again.");

        } else {
            for (Network network : business.getNetworkList()) {
                //System.err.println(network.getCountry());
                if (network.getCountry().equals(country)) {
                    JOptionPane.showMessageDialog(null, "This network already exists.");

                    return;
                }
            }
            try {
                int ok = 0;
                ok = JOptionPane.showConfirmDialog(null, "You cannot delete the network. Are you sure you want to proceed?");
                if (ok == 0) {
                    business.createCountry(country);

                    JOptionPane.showMessageDialog(null, "Network Added successfully");
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "There was error adding the network.");

            }

        }

    }//GEN-LAST:event_btnCountryActionPerformed

    private void cmbStateMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cmbStateMouseClicked
        
    }//GEN-LAST:event_cmbStateMouseClicked

    private void cmbStateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbStateActionPerformed

    }//GEN-LAST:event_cmbStateActionPerformed

    private void btnStateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnStateActionPerformed
        String country = (String) cmbCountry.getSelectedItem();
        String state = (String) cmbState.getSelectedItem();
        if (state == null) {
            JOptionPane.showMessageDialog(null, "Please select a state.");

        } else {

            for (Network countryNetwork : business.getNetworkList()) {
                if (countryNetwork.getCountry().equals(country)) {
                    flag = true;
                }
            }
            if(!flag){
                JOptionPane.showMessageDialog(null,"Please create a network at country level to proceed");

                return;
            }

            for (Network network : business.getNetworkList()) {
                //System.out.println("network.getCountry()" + network.getCountry() + "country" + (country));
                if (network.getCountry().equals(country)) {
                    for (Network stateNetwork : network.getNetWorkList()) {
                        //System.out.println("stateNetwork.getState()" + stateNetwork.getState() + "stateNetwork.getCountry()" + stateNetwork.getCountry() + "stateNetwork.getRole()" + stateNetwork.getRole());
                        if (stateNetwork.getState().equals(state) && stateNetwork.getCountry().equals(country) && stateNetwork.getRole().equals(Network.NetworkType.State)) {
                            JOptionPane.showMessageDialog(null, "This network already exists");

                            return;
                        }

                    }

                    business.createState(network, state);
                    JOptionPane.showMessageDialog(null, "Network Added successfully");

                }

            }
        }

    }//GEN-LAST:event_btnStateActionPerformed

    private void cmbCountryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCountryActionPerformed
        // TODO add your handling code here: 
    }//GEN-LAST:event_cmbCountryActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnCountry;
    private javax.swing.JButton btnReset;
    private javax.swing.JButton btnState;
    private javax.swing.JComboBox cmbCountry;
    private javax.swing.JComboBox cmbState;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    // End of variables declaration//GEN-END:variables
}
