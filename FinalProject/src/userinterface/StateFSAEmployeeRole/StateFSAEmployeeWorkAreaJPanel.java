/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.StateFSAEmployeeRole;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.TestSample.TestSample;
import Business.TestSample.TestSampleDirectory;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.FSAEmployeeWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author wuyue
 */
public class StateFSAEmployeeWorkAreaJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;    
    private Enterprise enterprise;
    private UserAccount userAccount;
    private Network network;
    private EcoSystem business;
    private Organization organization;
    public StateFSAEmployeeWorkAreaJPanel(JPanel userProcessContainer, Enterprise enterprise, UserAccount userAccount, Network network, EcoSystem business, Organization organization) {
        initComponents();
        this.userProcessContainer = userProcessContainer;        
        this.enterprise = enterprise;
        this.userAccount = userAccount;
        this.network = network;
        this.business = business;
        this.organization = organization;
        valueLabel.setText(enterprise.getName());
        setSize(700,700);
        //populatTable();
        
        for(WorkRequest wr : userAccount.getWorkQueue().getWorkRequestList()){
            int size = 0;
            if(wr.getStatus().equals("Processing")){
                int size1 = 0;
                for(WorkRequest wre : userAccount.getWorkQueue().getWorkRequestList()){
                    if(wre.getLinkID() == wr.getId()){
                        size += 1;
                        if(((FSAEmployeeWorkRequest) wre).getTestSampleDirectory().getTestSampleDirectory().size() != 0){
                            size1 += 1;
                        }
                    }
                    
                }
                if(size == size1 && size != 0){
                    for(WorkRequest wre : userAccount.getWorkQueue().getWorkRequestList()){
                        if(wre.getLinkID() == wr.getId()){
                            
                            if(((FSAEmployeeWorkRequest) wr).getTestSampleDirectory().getTestSampleDirectory().size() == 0){
                                TestSampleDirectory ts = new TestSampleDirectory();
                                ts.getTestSampleDirectory().add(((FSAEmployeeWorkRequest) wre).getTestSampleDirectory().getTestSampleDirectory().get(0));
                                ((FSAEmployeeWorkRequest) wr).setTestSampleDirectory(ts);
                            }
                            else{
                                ((FSAEmployeeWorkRequest) wr).getTestSampleDirectory().getTestSampleDirectory().add(((FSAEmployeeWorkRequest) wre).getTestSampleDirectory().getTestSampleDirectory().get(0));
                            }
                            
                        }
                        
                        
                    }
                    wr.setStatus("Samples collecting complete");
                }
            }
        }
        populatTable();
    }

    public void populatTable() {
        DefaultTableModel model = (DefaultTableModel) workRequestJTable.getModel();

        model.setRowCount(0);

        for (WorkRequest request : userAccount.getWorkQueue().getWorkRequestList()) {
            Object[] row = new Object[8];
            row[0] = request;
            row[1] = request.getMessage();
            row[2] = request.getSender();
            row[3] = request.getReceiver() == null ? null : request.getReceiver();
            row[4] = request.getReceiverNetwork();
            ArrayList<TestSample> sample = new ArrayList<>();
            sample = ((FSAEmployeeWorkRequest) request).getTestSampleDirectory().getTestSampleDirectory();
            if(sample.size() != 0){
                row[5] = sample;
                //ResultDirectory rd = new ResultDirectory();
                ArrayList<TestSample> sample1 = new ArrayList<>();
                for(TestSample ts : sample){
                    if(ts.getResult() != null){
                        //rd.getResultDiectory().add(ts.getResult());
                        sample1.add(ts);
                    }
                }
                if(sample1.size() == 0){
                    row[6] = null;
                }
                else{
                    row[6] = sample1;
                    if(sample.size() == sample1.size()){
                        request.setStatus("Completed");
                    }
                }
            }
            else{
                row[5] = null;
                row[6] = null;
            }
            
            row[7] = request.getStatus();

            model.addRow(row);
        }
    }
    

    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        enterpriseLabel = new javax.swing.JLabel();
        valueLabel = new javax.swing.JLabel();
        assignJButton = new javax.swing.JButton();
        resBtn = new javax.swing.JButton();
        labBtn = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        workRequestJTable = new javax.swing.JTable();

        enterpriseLabel.setFont(new java.awt.Font("Toppan Bunkyu Gothic", 1, 18)); // NOI18N
        enterpriseLabel.setText("Enterprise:");

        valueLabel.setFont(new java.awt.Font("Toppan Bunkyu Gothic", 0, 18)); // NOI18N
        valueLabel.setText("<value>");

        assignJButton.setText("Assign to me");
        assignJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignJButtonActionPerformed(evt);
            }
        });

        resBtn.setText("Send Requests to Restaurants >>");
        resBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resBtnActionPerformed(evt);
            }
        });

        labBtn.setText("Send Test Samples to Lab >>");
        labBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                labBtnActionPerformed(evt);
            }
        });

        workRequestJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "ID", "Message", "Sender", "Receiver", "Receiver Network", "Test Samples", "Result", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(workRequestJTable);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 800, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(enterpriseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(valueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(assignJButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(labBtn, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(resBtn, javax.swing.GroupLayout.Alignment.TRAILING))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(enterpriseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(valueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(43, 43, 43)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 196, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(resBtn)
                        .addGap(30, 30, 30)
                        .addComponent(labBtn))
                    .addComponent(assignJButton, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(26, 26, 26))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void assignJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assignJButtonActionPerformed

        int selectedRow = workRequestJTable.getSelectedRow();

        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Please select a row!");
            return;
        }
        
        WorkRequest request = (WorkRequest) workRequestJTable.getValueAt(selectedRow, 0);
        
        if(request.getSender() == userAccount){
            JOptionPane.showMessageDialog(null, "This work has already been assigned!");
            return;
        }
       
        if (request.getReceiver() != null) {
            JOptionPane.showMessageDialog(null, "This work has already been assigned!");
            return;
        }

        
        request.setReceiver(userAccount);
        request.setStatus("Pending");
        //System.out.println(request.getLinkID());
        populatTable();
    }//GEN-LAST:event_assignJButtonActionPerformed

    private void resBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resBtnActionPerformed

        int selectedRow = workRequestJTable.getSelectedRow();

        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Please select a row!");
            return;
        }
        

        
        WorkRequest request = (WorkRequest)workRequestJTable.getValueAt(selectedRow, 0);      
        
        
        for(WorkRequest wr : userAccount.getWorkQueue().getWorkRequestList()){
            if(request.getId() == wr.getLinkID()){
                JOptionPane.showMessageDialog(null, "You've already sent the request to restaurants!");
                return;
            }
        }
        
        
        if(request.getSender() == userAccount){
            JOptionPane.showMessageDialog(null, "This work has already been assigned!");
            return;
        }
        if (request.getReceiver() == null) {
            JOptionPane.showMessageDialog(null, "This work has not been assigned!");
            return;
        }
        if(request.getReceiver() != null & request.getReceiver() != userAccount){
            JOptionPane.showMessageDialog(null, "Sorry, this work has not been assigned to you!");
            return;
        }
        request.setStatus("Processing");

        StateFSAEmployeeWorkRequestJPanel doMEsWorkRequestJPanel = new StateFSAEmployeeWorkRequestJPanel(userProcessContainer, network, enterprise, userAccount, organization,request);
        //ProcessWorkRequestJPanel processWorkRequestJPanel = new ProcessWorkRequestJPanel(userProcessContainer, request);
        userProcessContainer.add("DoMEsWorkRequest", doMEsWorkRequestJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_resBtnActionPerformed

    private void labBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_labBtnActionPerformed
        // TODO add your handling code here:
        int selectedRow = workRequestJTable.getSelectedRow();

        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Please select a row!");
            return;
        }
        
        
        FSAEmployeeWorkRequest req = (FSAEmployeeWorkRequest) workRequestJTable.getValueAt(selectedRow, 0);
        
        if(req.getReceiver() != null & req.getReceiver() != userAccount){
            JOptionPane.showMessageDialog(null, "Sorry, this work has not been assigned to you!");
            return;
        }
//        if(req.getStatus().equals("Lab Test Completed")){
//            JOptionPane.showMessageDialog(null, "This work has already been completed by lab!");
//            return;
//        }
//        if(!req.getStatus().equals("Samples collecting complete")){
//            JOptionPane.showMessageDialog(null, "You haven't completed samples collecting process!");
//            return;
//        }


        int count = 0;
        for(WorkRequest wq: userAccount.getWorkQueue().getWorkRequestList()){
            if(req.getId() == wq.getLinkID()){
                count++;
            }
        }
        if(!(count != 0 && req.getTestSampleDirectory().getTestSampleDirectory().size() == count)){
            JOptionPane.showMessageDialog(null, "You haven't completed samples collecting process!");
            return;
        }
        if(req.getStatus().equals("Lab Test Completed") || req.getStatus().equals("Sending to lab")){
            JOptionPane.showMessageDialog(null, "Samples have already been sent to lab!");
            return;
        }
        
        
               
        TestSamplesJPanel doMEsWorkRequestJPanel = new TestSamplesJPanel(userProcessContainer, network, userAccount, req);
        //ProcessWorkRequestJPanel processWorkRequestJPanel = new ProcessWorkRequestJPanel(userProcessContainer, request);
        userProcessContainer.add("DoMEsWorkRequest", doMEsWorkRequestJPanel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_labBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton assignJButton;
    private javax.swing.JLabel enterpriseLabel;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton labBtn;
    private javax.swing.JButton resBtn;
    private javax.swing.JLabel valueLabel;
    private javax.swing.JTable workRequestJTable;
    // End of variables declaration//GEN-END:variables
}
