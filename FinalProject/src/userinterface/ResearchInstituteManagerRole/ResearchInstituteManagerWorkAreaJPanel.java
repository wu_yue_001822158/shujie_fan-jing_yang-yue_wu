/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.ResearchInstituteManagerRole;

import Business.Analysis.BarChart;
import Business.Analysis.QualifiedBarChart;
import Business.Analysis.TestResultBarChart;
import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.Patient.Patient;
import Business.Patient.PatientDirectory;
import Business.TestSample.TestSample;
import Business.TestSample.TestSampleDirectory;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.FSAEmployeeWorkRequest;
import Business.WorkQueue.ResearchInstituteWorkRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.awt.GridLayout;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;
import org.jfree.chart.ChartPanel;
import userinterface.NationalFSAManagerRole.ResultJPanel;

/**
 *
 * @author fanshujie
 */
public class ResearchInstituteManagerWorkAreaJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    
    private Enterprise enterprise;
    private UserAccount userAccount;
    private Network network;
    private EcoSystem business;
    private Organization organization;
    /**
     * Creates new form RestaurantWorkAreaJPanel
     */
    public ResearchInstituteManagerWorkAreaJPanel(JPanel userProcessContainer, UserAccount userAccount, Enterprise enterprise, Organization organization, Network network, EcoSystem business) {
        initComponents();       
        this.userProcessContainer = userProcessContainer;        
        this.enterprise = enterprise;
        this.userAccount = userAccount;
        this.network = network;
        this.business = business;
        this.organization = organization;
        
        valueLabel.setText(enterprise.getName());
        populatworkRequestTable();
        populateRIworkRequestTable();
    }

    public void populatworkRequestTable() {
        DefaultTableModel model = (DefaultTableModel) workRequestJTable.getModel();

        model.setRowCount(0);

        for (WorkRequest request : userAccount.getWorkQueue().getWorkRequestList()) {
            Object[] row = new Object[8];
            row[0] = request;
            row[1] = request.getMessage();
            row[2] = request.getSender();
            row[3] = request.getReceiver() == null ? null : request.getReceiver();
            row[4] = request.getReceiverNetwork();
            ArrayList<TestSample> sample = new ArrayList<>();
            sample = ((FSAEmployeeWorkRequest) request).getTestSampleDirectory().getTestSampleDirectory();
            if(sample.size() == 0){
                row[5] = null;
                row[6] = null;
            }
            else{                
                row[5] = sample;
                row[6] = sample;
            }
                    
            row[7] = request.getStatus();

            model.addRow(row);
        }
    }
    
    public void populateRIworkRequestTable(){
        DefaultTableModel model = (DefaultTableModel) RIworkRequestTbl.getModel();
        
        model.setRowCount(0);
        for (WorkRequest request : userAccount.getrIWorkQueue().getRIworkRequestList()){
            Object[] row = new Object[7];
            row[0] = request;
            row[1] = request.getMessage();
            row[2] = request.getSender();
            row[3] = request.getReceiver();
            row[4] = request.getReceiverNetwork();
            if(((ResearchInstituteWorkRequest)request).getPatientDirectory().getPatientDirectory().size() == 0){
                row[5] = null;
            }
            else{
                row[5] = ((ResearchInstituteWorkRequest)request).getPatientDirectory().getPatientDirectory();
            }
            
            
            row[6] = request.getStatus();
            model.addRow(row);
        }
    }
    
    
    public TestSampleDirectory collectSampleDirectory(){
        TestSampleDirectory tsd = new TestSampleDirectory();
        for (WorkRequest r : userAccount.getWorkQueue().getWorkRequestList()){
            for(TestSample ts : ((FSAEmployeeWorkRequest)r).getTestSampleDirectory().getTestSampleDirectory()){
                if(ts.getResult() != null && r.getSender() == userAccount){
                    
                    tsd.getTestSampleDirectory().add(ts);
                }
                
            }
        }
        return tsd;
    }
    
    
    public PatientDirectory collectPatientDirectory(){
        PatientDirectory tsd = new PatientDirectory();
        for (ResearchInstituteWorkRequest r : userAccount.getrIWorkQueue().getRIworkRequestList()){
            for(Patient ts : r.getPatientDirectory().getPatientDirectory()){
                if(r.getSender() == userAccount){
                    
                    tsd.getPatientDirectory().add(ts);
                }
                
            }
        }
        return tsd;
    }
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        enterpriseLabel = new javax.swing.JLabel();
        valueLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        RIworkRequestTbl = new javax.swing.JTable();
        sendBtn = new javax.swing.JButton();
        pdBtn = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        workRequestJTable = new javax.swing.JTable();
        sampleBtn = new javax.swing.JButton();
        anaBtn = new javax.swing.JButton();
        assignJButton = new javax.swing.JButton();
        processJButton = new javax.swing.JButton();

        enterpriseLabel.setFont(new java.awt.Font("Toppan Bunkyu Gothic", 1, 18)); // NOI18N
        enterpriseLabel.setText("Enterprise:");

        valueLabel.setFont(new java.awt.Font("Toppan Bunkyu Gothic", 0, 18)); // NOI18N
        valueLabel.setText("<value>");

        RIworkRequestTbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "ID", "Message", "Sender", "Receiver", "Receiver Network", "Patient Data", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(RIworkRequestTbl);

        sendBtn.setText("Send Request");
        sendBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendBtnActionPerformed(evt);
            }
        });

        pdBtn.setText("See the Full Patient Data >>");
        pdBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pdBtnActionPerformed(evt);
            }
        });

        workRequestJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "ID", "Message", "Sender", "Receiver", "Receiver Network", "Test Samples", "Result", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.String.class, java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(workRequestJTable);

        sampleBtn.setText("See the Full Test Sample Results >>");
        sampleBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sampleBtnActionPerformed(evt);
            }
        });

        anaBtn.setText("Analysis >>");
        anaBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                anaBtnActionPerformed(evt);
            }
        });

        assignJButton.setText("Assign to me");
        assignJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                assignJButtonActionPerformed(evt);
            }
        });

        processJButton.setText("Process >>");
        processJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                processJButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(16, 16, 16)
                                .addComponent(enterpriseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(assignJButton))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(sendBtn)))
                        .addGap(7, 7, 7)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(sampleBtn)
                                    .addComponent(anaBtn)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(valueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(609, 609, 609)
                                .addComponent(processJButton))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(pdBtn))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane1)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(enterpriseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(valueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addComponent(sampleBtn)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 125, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(pdBtn)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(assignJButton)
                    .addComponent(processJButton))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(sendBtn)
                    .addComponent(anaBtn))
                .addGap(21, 21, 21))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void sendBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sendBtnActionPerformed
        // TODO add your handling code here:
        RequestJPanel  mnjp = new RequestJPanel(userProcessContainer, organization, userAccount, network);
        userProcessContainer.add("NationalFSAManagerWorkRequest",mnjp);
        CardLayout layout=(CardLayout)userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_sendBtnActionPerformed

    private void sampleBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sampleBtnActionPerformed
        // TODO add your handling code here:
        int selectedRow = workRequestJTable.getSelectedRow();

        if (selectedRow < 0) {           
            JOptionPane.showMessageDialog(null, "Please select a row!");
            return;            
        }

        FSAEmployeeWorkRequest request = (FSAEmployeeWorkRequest) workRequestJTable.getValueAt(selectedRow, 0);
        
        if(request.getTestSampleDirectory().getTestSampleDirectory().size() == 0){
            JOptionPane.showMessageDialog(null, "There are no results available!");
            return;
        }
        ResultJPanel mnjp = new ResultJPanel(userProcessContainer, request.getTestSampleDirectory());
        userProcessContainer.add("ResultJPanel",mnjp);
        CardLayout layout=(CardLayout)userProcessContainer.getLayout();
        layout.next(userProcessContainer);
        
    }//GEN-LAST:event_sampleBtnActionPerformed

    private void pdBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pdBtnActionPerformed
        // TODO add your handling code here:
        int selectedRow = RIworkRequestTbl.getSelectedRow();

        if (selectedRow < 0) {           
            JOptionPane.showMessageDialog(null, "Please select a row!");
            return;            
        }
        
        ResearchInstituteWorkRequest request = (ResearchInstituteWorkRequest) RIworkRequestTbl.getValueAt(selectedRow, 0);
        if(request.getPatientDirectory().getPatientDirectory().size() == 0){
            JOptionPane.showMessageDialog(null, "There are no results available!");
            return;
        }
        ViewPatientDataJPanel  mnjp = new ViewPatientDataJPanel(userProcessContainer, request);
        userProcessContainer.add("ViewPatientDataJPanel",mnjp);
        CardLayout layout=(CardLayout)userProcessContainer.getLayout();
        layout.next(userProcessContainer);
    }//GEN-LAST:event_pdBtnActionPerformed

    private void assignJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_assignJButtonActionPerformed

        int selectedRow = workRequestJTable.getSelectedRow();
        int Row = RIworkRequestTbl.getSelectedRow();

        if (selectedRow < 0 && Row < 0) {
            JOptionPane.showMessageDialog(null, "Please select a row!");
            return;
        }
        
               
        
        if(selectedRow >= 0){
            WorkRequest request = (WorkRequest) workRequestJTable.getValueAt(selectedRow, 0);
            if(request.getSender() == userAccount || request.getReceiver() != null){
                JOptionPane.showMessageDialog(null, "This work has already been assigned!");
                return;
            }
            request.setReceiver(userAccount);
            request.setStatus("Pending");
            
            ResearchInstituteWorkRequest req = userAccount.getrIWorkQueue().searchRequest(request.getId());
            req.setReceiver(userAccount);
            req.setStatus("Pending");
            
            populatworkRequestTable();
            populateRIworkRequestTable();
            
            return;
        }
        
        if(Row >= 0){
            ResearchInstituteWorkRequest req = (ResearchInstituteWorkRequest) RIworkRequestTbl.getValueAt(Row, 0);
            if(req.getSender() == userAccount || req.getReceiver() != null){
                JOptionPane.showMessageDialog(null, "This work has already been assigned!");
                return;
            }
            req.setReceiver(userAccount);
            req.setStatus("Pending");
            
            WorkRequest request = userAccount.getWorkQueue().searchRequest(req.getId());
            request.setReceiver(userAccount);
            request.setStatus("Pending");
        }

        
        populatworkRequestTable();
        populateRIworkRequestTable();
    }//GEN-LAST:event_assignJButtonActionPerformed

    private void processJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_processJButtonActionPerformed

        int selectedRow = workRequestJTable.getSelectedRow();
        int Row = RIworkRequestTbl.getSelectedRow();

        if (selectedRow < 0 && Row < 0) {
            JOptionPane.showMessageDialog(null, "Please select a row!");
            return;
        }
        
        TestSampleDirectory tsd = collectSampleDirectory();
        PatientDirectory pd = collectPatientDirectory();
        if(tsd.getTestSampleDirectory().size() == 0 || pd.getPatientDirectory().size() == 0){
            JOptionPane.showMessageDialog(null, "There are no datas available!");
            return;
        }
                     
        if(selectedRow >= 0){
            FSAEmployeeWorkRequest request = (FSAEmployeeWorkRequest) workRequestJTable.getValueAt(selectedRow, 0);
            if(request.getReceiver() != userAccount){
                JOptionPane.showMessageDialog(null, "This work has not been assigned to you!");
                return;
            }
            
            
            request.setTestSampleDirectory(tsd);
            request.setStatus("Completed");
            
            
            ResearchInstituteWorkRequest req = userAccount.getrIWorkQueue().searchRequest(request.getId());
            req.setPatientDirectory(pd);
            req.setStatus("Completed");
            JOptionPane.showMessageDialog(null, "Send data to International Research Institute successfully!");
            
            populatworkRequestTable();
            populateRIworkRequestTable();
            
            return;
        }
        
        if(Row >= 0){
            ResearchInstituteWorkRequest req = (ResearchInstituteWorkRequest) RIworkRequestTbl.getValueAt(Row, 0);
            if(req.getReceiver() != userAccount){
                JOptionPane.showMessageDialog(null, "This work has not been assigned to you!");
                return;
            }
            req.setPatientDirectory(pd);
            req.setStatus("Completed");
            
            FSAEmployeeWorkRequest request = (FSAEmployeeWorkRequest) userAccount.getWorkQueue().searchRequest(req.getId());
            
            request.setTestSampleDirectory(tsd);
            request.setStatus("Completed");
            
                       
            JOptionPane.showMessageDialog(null, "Send data to International Research Institute successfully!");
            
            populatworkRequestTable();
            populateRIworkRequestTable();
        }
    }//GEN-LAST:event_processJButtonActionPerformed

    private void anaBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_anaBtnActionPerformed
        // TODO add your handling code here:
        TestSampleDirectory tsd = new TestSampleDirectory();
        PatientDirectory pd = new PatientDirectory();
        PatientDirectory pd1 = new PatientDirectory();
        for(WorkRequest wr : userAccount.getWorkQueue().getWorkRequestList()){
            if(wr.getSender() == userAccount && ((FSAEmployeeWorkRequest)wr).getTestSampleDirectory().getTestSampleDirectory().size() != 0){
                for(TestSample ts : ((FSAEmployeeWorkRequest)wr).getTestSampleDirectory().getTestSampleDirectory()){
                    if(!tsd.getTestSampleDirectory().contains(ts)){
                        tsd.getTestSampleDirectory().add(ts);
                    }
                }               
            }
        }
        //System.out.println(tsd.getTestSampleDirectory());
        
        for(ResearchInstituteWorkRequest wr : userAccount.getrIWorkQueue().getRIworkRequestList()){
            if(wr.getSender() == userAccount && wr.getPatientDirectory().getPatientDirectory().size() != 0){
                for(Patient p : wr.getPatientDirectory().getPatientDirectory()){
                    if(!pd.getPatientDirectory().contains(p)){
                        pd.getPatientDirectory().add(p);
                    }
                }
            }
        }
        //System.out.println(pd.getPatientDirectory());
        
        if(tsd.getTestSampleDirectory().size() == 0 && pd.getPatientDirectory().size() == 0){
            JOptionPane.showMessageDialog(null, "There is no data to analyze!");
            return;
        }
        
        for(Patient p : pd.getPatientDirectory()){
            if(p.getDisease().equalsIgnoreCase("paratyphoid") || 
                    p.getDisease().equalsIgnoreCase("acute enteritis") || 
                    p.getDisease().equalsIgnoreCase("anisakiasis") || 
                    p.getDisease().equalsIgnoreCase("cryptococcosis") || 
                    p.getDisease().equalsIgnoreCase("proteus bacillus") || 
                    p.getDisease().equalsIgnoreCase("ascariasis")){
                pd1.getPatientDirectory().add(p);
            }
        }
        
        
        
        AnalysisJPanel mnjp = new AnalysisJPanel(userProcessContainer, tsd, pd);
        
        mnjp.setLayout(new GridLayout(2,2,10,10));
        userProcessContainer.add("ResultJPanel",mnjp);
        ChartPanel c = new BarChart(pd, pd1).getChartPanel();
        mnjp.add("ResultJPanel",c);
        ChartPanel d = new QualifiedBarChart(business, tsd, pd, pd1).getChartPanel();
        mnjp.add("ResultJPanel",d);        
        ChartPanel e = new QualifiedBarChart(business, tsd, pd, pd1).getChartPanel();
        mnjp.add("ResultJPanel",e);
        ChartPanel f = new TestResultBarChart(tsd).getChartPanel();
        mnjp.add("ResultJPanel",f);
        CardLayout layout=(CardLayout)userProcessContainer.getLayout();
        layout.next(userProcessContainer);
        
    }//GEN-LAST:event_anaBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable RIworkRequestTbl;
    private javax.swing.JButton anaBtn;
    private javax.swing.JButton assignJButton;
    private javax.swing.JLabel enterpriseLabel;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JButton pdBtn;
    private javax.swing.JButton processJButton;
    private javax.swing.JButton sampleBtn;
    private javax.swing.JButton sendBtn;
    private javax.swing.JLabel valueLabel;
    private javax.swing.JTable workRequestJTable;
    // End of variables declaration//GEN-END:variables
}
