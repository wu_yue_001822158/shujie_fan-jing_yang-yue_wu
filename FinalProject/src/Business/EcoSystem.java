/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.MedicalDepartmentOrganization;
import Business.Organization.Organization;
import Business.Patient.Patient;
import Business.Role.Role;
import Business.Role.SystemAdminRole;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.time;
import java.util.ArrayList;

/**
 *
 * @author fanshujie
 */
public class EcoSystem extends Organization {
    private static EcoSystem business;
    private ArrayList<Network> networkList;

    public static EcoSystem getInstance() {
        if (business == null) {
            business = new EcoSystem();
        }
        return business;
    }

    private EcoSystem() {
        super(null);
        networkList = new ArrayList<>();
    }

    public ArrayList<Network> getNetworkList() {
        return networkList;
    }

//    public Network createAndAddNetwork() {
//        Network network = new Network();
//        networkList.add(network);
//        return network;
//    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roleList = new ArrayList<>();
        roleList.add(new SystemAdminRole());
        return roleList;
    }
    
   public Network createInternational(String name){
        Network International = new Network();
        International.setCountry("");
        International.setName(name);
        International.setRole(Network.NetworkType.International);
        networkList.add(International);
        return International;
    }
    
     public Network createCountry(String name){
        Network country = new Network();
        country.setCountry(name);
        country.setName(name);
        country.setRole(Network.NetworkType.Country);
        networkList.add(country);
        return country;
    }
   
    public Network createState(Network country, String name){
        Network state = new Network();
        state.setCountry(country.getCountry());
        state.setState(name);
        state.setName(name);
        state.setRole(Network.NetworkType.State);
        country.getNetWorkList().add(state);
        return state;
    }   
    
    
    
    

    public boolean checkIfUsernameIsUnique(String username) {

        if (!this.getUserAccountDirectory().checkIfUsernameIsUnique(username)) {
            return false;
        }
        for(Network network : networkList){
            for(Enterprise e : network.getEnterpriseDirectory().getEnterpriseList()){
                if(!e.getUserAccountDirectory().checkIfUsernameIsUnique(username)){
                    return false;
                }
                for(Organization o : e.getOrganizationDirectory().getOrganizationList()){
                    if(!o.getUserAccountDirectory().checkIfUsernameIsUnique(username)){
                        return false;
                    }
                }
            }
            for(Network stateNetwork: network.getNetWorkList()){
                for(Enterprise e1 : stateNetwork.getEnterpriseDirectory().getEnterpriseList()){
                if(!e1.getUserAccountDirectory().checkIfUsernameIsUnique(username)){
                    return false;
                }
                for(Organization o1 : e1.getOrganizationDirectory().getOrganizationList()){
                    if(!o1.getUserAccountDirectory().checkIfUsernameIsUnique(username)){
                        return false;
                    }
                }
            }
                
            }
        }
        return true;
    }
    
    public Network findNetwork(Enterprise enterprise){
        for(Network network : networkList){
            for(Enterprise e : network.getEnterpriseDirectory().getEnterpriseList()){
                if(e == enterprise){
                    return network;
                }
            }
            for(Network n : network.getNetWorkList()){
                for(Enterprise e : n.getEnterpriseDirectory().getEnterpriseList()){
                    if(e == enterprise){
                        return n;
                    }
                }
            }
        }
        return null;
    }
    
    
    public Network findCountryNetwork(Enterprise enterprise){
        for(Network network : networkList){
            for(Enterprise e : network.getEnterpriseDirectory().getEnterpriseList()){
                if(e == enterprise){
                    return network;
                }
                for(Network n : network.getNetWorkList()){
                    for(Enterprise en : n.getEnterpriseDirectory().getEnterpriseList()){
                        if(en == enterprise){
                            return network;
                        }
                    }
                }
            }
            
        }
        return null;
    }
    
    public Network findCountryNetworkForPatient(Patient p){
        for(Network network : networkList){
            for(Network n : network.getNetWorkList()){
                for(Enterprise e : n.getEnterpriseDirectory().getEnterpriseList()){
                    if(e.getEnterpriseType().toString().equals("Medical Department")){
                        for(Organization o : e.getOrganizationDirectory().getOrganizationList()){
                            for(Patient patient : ((MedicalDepartmentOrganization)o).getPatientDirectory().getPatientDirectory()){
                                if(patient == p){
                                    return network;
                                }
                            }
                        }
                    }
                }
            }           
        }
        return null;
    }
    
    
    public Network findNetworkForPatient(Patient p){
        for(Network network : networkList){
            for(Network n : network.getNetWorkList()){
                for(Enterprise e : n.getEnterpriseDirectory().getEnterpriseList()){
                    if(e.getEnterpriseType().toString().equals("Medical Department")){
                        for(Organization o : e.getOrganizationDirectory().getOrganizationList()){
                            for(Patient patient : ((MedicalDepartmentOrganization)o).getPatientDirectory().getPatientDirectory()){
                                if(patient == p){
                                    return n;
                                }
                            }
                        }
                    }
                }
            }           
        }
        return null;
    }
    
}
