/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Network;

import Business.Enterprise.Enterprise;
import Business.Enterprise.EnterpriseDirectory;
import java.util.ArrayList;

/**
 *
 * @author fanshujie
 */
public class Network {

    private String country;
    private String state;
    private String name;
    private EnterpriseDirectory enterpriseDirectory;
    private NetworkType role;
    private ArrayList<Network> netWorkList;

    public enum NetworkType {
        Country("Country"),
        State("State"),
        International("International");

        private String value;

        private NetworkType(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    public Network() {
        netWorkList = new ArrayList<>();
        enterpriseDirectory = new EnterpriseDirectory();
    }

    public EnterpriseDirectory getEnterpriseDirectory() {
        return enterpriseDirectory;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public NetworkType getRole() {
        return role;
    }

    public void setRole(NetworkType role) {
        this.role = role;
    }

    public ArrayList<Network> getNetWorkList() {
        return netWorkList;
    }
    

       
    @Override
   public String toString(){
        return this.getName(); 
    }
   
}
