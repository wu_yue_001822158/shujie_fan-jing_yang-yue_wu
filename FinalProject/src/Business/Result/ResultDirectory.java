/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Result;

import java.util.ArrayList;

/**
 *
 * @author fanshujie
 */
public class ResultDirectory {
    private ArrayList<Result> resultDiectory;
    
    public ResultDirectory(){
        resultDiectory = new ArrayList<>();
    }

    public ArrayList<Result> getResultDiectory() {
        return resultDiectory;
    }

    public void setResultDiectory(ArrayList<Result> resultDiectory) {
        this.resultDiectory = resultDiectory;
    }
    
    
}
