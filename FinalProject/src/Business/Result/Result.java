/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Result;

/**
 *
 * @author fanshujie
 */
public class Result {
    private int bacterialCount;
    private String bacterialEvaluate;
    private int coliformGroup;
    private String coliformEvaluate;
    private int mold;
    private String moldEvaluate;
    private double glycine;
    private String glycineEvaluate;
    private double ammoniumPhosphatide;
    private String apEvaluate;
    private double liquidParaffin;
    private String lpEvaluate;
    private double hg;
    private String hgEvaluate;
    private double as;
    private String asEvaluate;
    private double ph;
    private String phEvaluate;
    private double cd;
    private String cdEvaluate;
    private int fe;
    private String feEvaluate;
    private double se;
    private String seEvaluate;
    private double dichlorvos;
    private String dichlorvosEvaluate;
    private double fenthion;
    private String fenthionEvaluate;
    private String evaluate;

    public int getBacterialCount() {
        return bacterialCount;
    }

    public void setBacterialCount(int bacterialCount) {
        this.bacterialCount = bacterialCount;
        if(bacterialCount < 10000){
            setBacterialEvaluate("Qualified");
        }
        else{
            setBacterialEvaluate("Unqualified");
        }
    }
    
    
    public String getBacterialEvaluate() {
        return bacterialEvaluate;
    }

    public void setBacterialEvaluate(String bacterialEvaluate) {
        this.bacterialEvaluate = bacterialEvaluate;
    }

    public int getColiformGroup() {
        return coliformGroup;
    }

    public void setColiformGroup(int coliformGroup) {
        this.coliformGroup = coliformGroup;
        if(coliformGroup < 30){
            setColiformEvaluate("Qualified");
        }
        else{
            setColiformEvaluate("Unqualified");
        }
    }



    public String getColiformEvaluate() {
        return coliformEvaluate;
    }
    
    

    public void setColiformEvaluate(String coliformEvaluate) {
        this.coliformEvaluate = coliformEvaluate;
    }

    public int getMold() {
        return mold;
    }

    public void setMold(int mold) {
        this.mold = mold;
        if(mold < 20){
            setMoldEvaluate("Qualified");
        }
        else{
            setMoldEvaluate("Unqualified");
        }
    }

    public String getMoldEvaluate() {
        return moldEvaluate;
    }

    public void setMoldEvaluate(String moldEvaluate) {
        this.moldEvaluate = moldEvaluate;
    }

    public double getGlycine() {
        return glycine;
    }

    public void setGlycine(double glycine) {
        this.glycine = glycine;
        if(glycine < 3){
            setGlycineEvaluate("Qualified");
        }
        else{
            setGlycineEvaluate("Unqualified");
        }
    }

    public String getGlycineEvaluate() {
        return glycineEvaluate;
    }

    public void setGlycineEvaluate(String glycineEvaluate) {
        this.glycineEvaluate = glycineEvaluate;
    }

    public double getAmmoniumPhosphatide() {
        return ammoniumPhosphatide;
    }

    public void setAmmoniumPhosphatide(double ammoniumPhosphatide) {
        this.ammoniumPhosphatide = ammoniumPhosphatide;
        if(ammoniumPhosphatide < 10){
            setApEvaluate("Qualified");
        }
        else{
            setApEvaluate("Unqualified");
        }
    }

    public String getApEvaluate() {
        return apEvaluate;
    }

    public void setApEvaluate(String apEvaluate) {
        this.apEvaluate = apEvaluate;
    }

    public double getLiquidParaffin() {
        return liquidParaffin;
    }

    public void setLiquidParaffin(double liquidParaffin) {
        this.liquidParaffin = liquidParaffin;
        if(liquidParaffin < 5){
            setLpEvaluate("Qualified");
        }
        else{
            setLpEvaluate("Unqualified");
        }
    }

    public String getLpEvaluate() {
        return lpEvaluate;
    }

    public void setLpEvaluate(String lpEvaluate) {
        this.lpEvaluate = lpEvaluate;
    }

    public double getHg() {
        return hg;
    }

    public void setHg(double hg) {
        this.hg = hg;
        if(hg < 0.01){
            setHgEvaluate("Qualified");
        }
        else{
            setHgEvaluate("Unqualified");
        }
    }

    public String getHgEvaluate() {
        return hgEvaluate;
    }

    public void setHgEvaluate(String hgEvaluate) {
        this.hgEvaluate = hgEvaluate;
    }

    public double getAs() {
        return as;
    }

    public void setAs(double as) {
        this.as = as;
        if(as < 0.5){
            setAsEvaluate("Qualified");
        }
        else{
            setAsEvaluate("Unqualified");
        }
    }

    public String getAsEvaluate() {
        return asEvaluate;
    }

    public void setAsEvaluate(String asEvaluate) {
        this.asEvaluate = asEvaluate;
    }

    public double getPh() {
        return ph;
    }

    public void setPh(double ph) {
        this.ph = ph;
        if(ph < 0.2){
            setPhEvaluate("Qualified");
        }
        else{
            setPhEvaluate("Unqualified");
        }
    }

    public String getPhEvaluate() {
        return phEvaluate;
    }

    public void setPhEvaluate(String phEvaluate) {
        this.phEvaluate = phEvaluate;
    }

    public double getCd() {
        return cd;
    }

    public void setCd(double cd) {
        this.cd = cd;
        if(cd < 0.03){
            setCdEvaluate("Qualified");
        }
        else{
            setCdEvaluate("Unqualified");
        }
    }

    public String getCdEvaluate() {
        return cdEvaluate;
    }

    public void setCdEvaluate(String cdEvaluate) {
        this.cdEvaluate = cdEvaluate;
    }

    public int getFe() {
        return fe;
    }

    public void setFe(int fe) {
        this.fe = fe;
        if(fe < 20){
            setFeEvaluate("Qualified");
        }
        else{
            setFeEvaluate("Unqualified");
        }
    }

    public String getFeEvaluate() {
        return feEvaluate;
    }

    public void setFeEvaluate(String feEvaluate) {
        this.feEvaluate = feEvaluate;
    }

    public double getSe() {
        return se;
    }

    public void setSe(double se) {
        this.se = se;
        if(se < 0.5){
            setSeEvaluate("Qualified");
        }
        else{
            setSeEvaluate("Unqualified");
        }
    }

    public String getSeEvaluate() {
        return seEvaluate;
    }

    public void setSeEvaluate(String seEvaluate) {
        this.seEvaluate = seEvaluate;
    }

    public double getDichlorvos() {
        return dichlorvos;
    }

    public void setDichlorvos(double dichlorvos) {
        this.dichlorvos = dichlorvos;
        if(dichlorvos < 0.004){
            setDichlorvosEvaluate("Qualified");
        }
        else{
            setDichlorvosEvaluate("Unqualified");
        }
    }

    public String getDichlorvosEvaluate() {
        return dichlorvosEvaluate;
    }

    public void setDichlorvosEvaluate(String dichlorvosEvaluate) {
        this.dichlorvosEvaluate = dichlorvosEvaluate;
    }

    public double getFenthion() {
        return fenthion;
    }

    public void setFenthion(double fenthion) {
        this.fenthion = fenthion;
        if(fenthion < 0.007){
            setFenthionEvaluate("Qualified");
        }
        else{
            setFenthionEvaluate("Unqualified");
        }
    }

    public String getFenthionEvaluate() {
        return fenthionEvaluate;
    }

    public void setFenthionEvaluate(String fenthionEvaluate) {
        this.fenthionEvaluate = fenthionEvaluate;
    }
    
    

    public String getEvaluate() {
        return evaluate;
    }

    public void setEvaluate(String evaluate) {
        this.evaluate = evaluate;
    }
    
    
    public String configEvaluate(int bacterialCount, int coliformGroup){
        if(bacterialCount < 10000 && coliformGroup < 30){
            return "Qualified";
        }
        return "Unqualified";
    }
    
    
    public String configureEvaluate(int bacterialCount, int coliformGroup, int mold,
                                double glycine, double ammoniumPhosphatide, double liquidParaffin,
                                double hg, double as, double ph, double cd, int fe, double se, double dichlorvos,
                                double fenthion){
        if(bacterialCount < 10000 && coliformGroup < 30 && mold < 20 && glycine < 3 && ammoniumPhosphatide <10
           && liquidParaffin <5 && hg <0.01 && as <0.5 && ph <0.2 && cd <0.03 && fe <20 && se < 0.5 && dichlorvos <0.004
           && fenthion < 0.007){
            return "Qualified";
        }
        return "Unqualified";
    }
    
}
