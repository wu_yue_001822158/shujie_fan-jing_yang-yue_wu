/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import static Business.Enterprise.Enterprise.EnterpriseType.InternationalResearchInstitute;
import java.util.ArrayList;

/**
 *
 * @author fanshujie
 */
public class EnterpriseDirectory {
    private ArrayList<Enterprise> enterpriseList;

    public EnterpriseDirectory() {
        enterpriseList = new ArrayList<>();
    }

    public ArrayList<Enterprise> getEnterpriseList() {
        return enterpriseList;
    }
    
    public Enterprise createAndAddEnterprise(String name, Enterprise.EnterpriseType type){
        Enterprise enterprise = null;
        if (type == Enterprise.EnterpriseType.InternationalResearchInstitute){
            enterprise = new InternationalResearchInstituteEnterprise(name);
            enterpriseList.add(enterprise);
        }
        if (type == Enterprise.EnterpriseType.ResearchInstitute){
            enterprise = new ResearchInstituteEnterprise(name);
            enterpriseList.add(enterprise);
        }
        if (type == Enterprise.EnterpriseType.NationalFoodSecurityAuthority){
            enterprise = new NationalFoodSecurityAuthorityEnterprise(name);
            enterpriseList.add(enterprise);
        }
        if (type == Enterprise.EnterpriseType.StateFoodSecurityAuthority){
            enterprise = new StateFoodSecurityAuthorityEnterprise(name);
            enterpriseList.add(enterprise);
        }
        if (type == Enterprise.EnterpriseType.MedicalDepartment){
            enterprise = new MedicalDepartmentEnterprise(name);
            enterpriseList.add(enterprise);
        }
        if (type == Enterprise.EnterpriseType.Restaurant){
            enterprise = new RestaurantEnterprise(name);
            enterpriseList.add(enterprise);
        }
        return enterprise;
    }
}
