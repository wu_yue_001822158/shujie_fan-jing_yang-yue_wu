/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author fanshujie
 */
public class InternationalResearchInstituteEnterprise extends Enterprise {
    public InternationalResearchInstituteEnterprise(String name) {
        super(name, Enterprise.EnterpriseType.InternationalResearchInstitute);
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        return null;
    }
}
