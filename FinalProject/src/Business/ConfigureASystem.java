/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Employee.Employee;
import Business.Network.Network;
import Business.Role.NationalFSAManagerRole;
import Business.Role.StateFSAManagerRole;
import Business.Role.SystemAdminRole;
import Business.UserAccount.UserAccount;

/**
 *
 * @author fanshujie
 */
public class ConfigureASystem {
    public static EcoSystem configure(){
        
        EcoSystem system = EcoSystem.getInstance();
        
        //Create a network
        //create an enterprise
        //initialize some organizations
        //have some employees 
        //create user account
        
        
        Employee employee = system.getEmployeeDirectory().createEmployee("RRH");
//        Network International = new Network();
//        International.setCountry(" ");
//        International.setName("International");
//        International.setRole(Network.NetworkType.International);
//        system.getNetworkList().add(International);
        Network International = system.createInternational("International");
        
        
        UserAccount ua = system.getUserAccountDirectory().createUserAccount("sysadmin", "sysadmin", employee, new SystemAdminRole());
//        UserAccount u = system.getUserAccountDirectory().createUserAccount("aa", "aa", employee, new NationalFSAManagerRole());
//        UserAccount a = system.getUserAccountDirectory().createUserAccount("bb", "bb", employee, new StateFSAManagerRole());
        return system;
    }
}
