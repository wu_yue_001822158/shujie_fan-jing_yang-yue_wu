/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Analysis;

import Business.Patient.Patient;
import Business.Patient.PatientDirectory;
import java.util.ArrayList;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author fanshujie
 */
public class BarChart {
    ChartPanel frame1;
    
    public BarChart(PatientDirectory pd, PatientDirectory pd1){  
        CategoryDataset dataset = getDataSet(pd, pd1);
        
        JFreeChart chart = ChartFactory.createBarChart(  
                             "Patient Data", // 图表标题  
                            "Disease", // 目录轴的显示标签  
                            "Total Number Percentage", // 数值轴的显示标签  
                            dataset, // 数据集  
                            PlotOrientation.VERTICAL, // 图表方向：水平、垂直  
                            true,           // 是否显示图例(对于简单的柱状图必须是false)  
                            false,          // 是否生成工具  
                            false           // 是否生成URL链接  
                            );  
           
            
         frame1=new ChartPanel(chart,true);        //这里也可以用chartFrame,可以直接生成一个独立的Frame  
           
    }  
    
    private static CategoryDataset getDataSet(PatientDirectory pd, PatientDirectory pd1) {  
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();   
        ArrayList<String> diseaseList = new ArrayList<>();
        diseaseList.add("paratyphoid");
        diseaseList.add("acute enteritis");
        diseaseList.add("anisakiasis");
        diseaseList.add("cryptococcosis");
        diseaseList.add("proteus bacillus");
        diseaseList.add("ascariasis");
//        System.out.println(pd.getPatientDirectory().size());
//        System.out.println(pd1.getPatientDirectory().size());
//        for(Patient p : pd.getPatientDirectory()){               
//            if(!diseaseList.contains(p.getDisease())){
//                diseaseList.add(p.getDisease());
//            }               
//        }
        for(int i = 0; i < diseaseList.size(); i++){              
            dataset.addValue(count(pd1, diseaseList.get(i))/pd1.getPatientDirectory().size(), diseaseList.get(i), "percentage");
        }

        return dataset;  
    }  
    
    
    public static double count(PatientDirectory pd1, String disease){
        int i = 0;
        for(Patient p : pd1.getPatientDirectory()){ 
            if(p.getDisease().equalsIgnoreCase(disease)){
                i += 1;
            }
        }
        return i;
    }
    
    
    public ChartPanel getChartPanel(){  
        return frame1;  

    }  
}
