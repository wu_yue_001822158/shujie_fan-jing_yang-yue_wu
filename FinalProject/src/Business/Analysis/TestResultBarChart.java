/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Analysis;

import Business.TestSample.TestSample;
import Business.TestSample.TestSampleDirectory;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author fanshujie
 */
public class TestResultBarChart {
    ChartPanel frame1;
    
    public TestResultBarChart(TestSampleDirectory tsd){  
        CategoryDataset dataset = getDataSet(tsd);
        
        JFreeChart chart = ChartFactory.createBarChart(  
                             "Test Sample Result", // 图表标题  
                            "Testing Index", // 目录轴的显示标签  
                            "Total Number Percentage", // 数值轴的显示标签  
                            dataset, // 数据集  
                            PlotOrientation.VERTICAL, // 图表方向：水平、垂直  
                            true,           // 是否显示图例(对于简单的柱状图必须是false)  
                            false,          // 是否生成工具  
                            false           // 是否生成URL链接  
                            );  
           
            
         frame1=new ChartPanel(chart,true);        //这里也可以用chartFrame,可以直接生成一个独立的Frame  
           
    }  
    
    private static CategoryDataset getDataSet(TestSampleDirectory tsd) {  
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();   

//        dataset.addValue(BacterialCount(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "percentage", "BacterialCount");
//        dataset.addValue(ColiformGroup(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "percentage", "ColiformGroup");
//        dataset.addValue(Mold(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "percentage", "Mold");
//        dataset.addValue(Glycine(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "percentage", "Glycine");        
//        dataset.addValue(AmmoniumPhosphatide(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "percentage", "Ammonium Phosphatide");
//        dataset.addValue(Glycine(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "percentage", "Glycine");
//        dataset.addValue(LiquidParaffin(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "percentage", "LiquidParaffin");
//        dataset.addValue(Hg(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "percentage", "Hg");
//        dataset.addValue(As(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "percentage", "As");
//        dataset.addValue(Cd(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "percentage", "Cd");
//        dataset.addValue(Fe(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "percentage", "Fe");
//        dataset.addValue(Se(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "percentage", "Se");
//        dataset.addValue(Dichlorvos(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "percentage", "Fichlorvos");
//        dataset.addValue(Fenthion(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "percentage", "Fenthion");
    

        dataset.addValue(BacterialCount(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "BacterialCount", "percentage");
        dataset.addValue(ColiformGroup(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "ColiformGroup", "percentage");
        dataset.addValue(Mold(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "Mold", "percentage");
        dataset.addValue(Glycine(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "Glycine", "percentage");        
        dataset.addValue(AmmoniumPhosphatide(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "Ammonium Phosphatide", "percentage");
        dataset.addValue(LiquidParaffin(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "Liquid Paraffin", "percentage");
        dataset.addValue(Hg(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "Hg", "percentage");
        dataset.addValue(As(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "As", "percentage");
        dataset.addValue(Ph(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "Ph", "percentage");
        dataset.addValue(Cd(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "Cd", "percentage");
        dataset.addValue(Fe(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "Fe", "percentage");
        dataset.addValue(Se(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "Se", "percentage");
        dataset.addValue(Dichlorvos(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "Fichlorvos", "percentage");
        dataset.addValue(Fenthion(tsd, "Unqualified")/tsd.getTestSampleDirectory().size(), "Fenthion", "percentage");
        
        
        return dataset;  
    }  
    
    
    public static double BacterialCount(TestSampleDirectory tsd, String evaluate){
        int i = 0;
        for(TestSample p : tsd.getTestSampleDirectory()){ 
            if(p.getResult().getBacterialEvaluate().equals(evaluate)){
                i += 1;
            }
        }
        return i;
    }
    
    public static double ColiformGroup(TestSampleDirectory tsd, String evaluate){
        int i = 0;
        for(TestSample p : tsd.getTestSampleDirectory()){ 
            if(p.getResult().getColiformEvaluate().equals(evaluate)){
                i += 1;
            }
        }
        return i;
    }
    
    
    public static double Mold(TestSampleDirectory tsd, String evaluate){
        int i = 0;
        for(TestSample p : tsd.getTestSampleDirectory()){ 
            if(p.getResult().getMoldEvaluate().equals(evaluate)){
                i += 1;
            }
        }
        return i;
    }
    
    
    
    public static double Glycine(TestSampleDirectory tsd, String evaluate){
        int i = 0;
        for(TestSample p : tsd.getTestSampleDirectory()){ 
            if(p.getResult().getGlycineEvaluate().equals(evaluate)){
                i += 1;
            }
        }
        return i;
    }
    
    public static double AmmoniumPhosphatide(TestSampleDirectory tsd, String evaluate){
        int i = 0;
        for(TestSample p : tsd.getTestSampleDirectory()){ 
            if(p.getResult().getApEvaluate().equals(evaluate)){
                i += 1;
            }
        }
        return i;
    }
    
    
    public static double LiquidParaffin(TestSampleDirectory tsd, String evaluate){
        int i = 0;
        for(TestSample p : tsd.getTestSampleDirectory()){ 
            if(p.getResult().getLpEvaluate().equals(evaluate)){
                i += 1;
            }
        }
        return i;
    }
    
    
    public static double Hg(TestSampleDirectory tsd, String evaluate){
        int i = 0;
        for(TestSample p : tsd.getTestSampleDirectory()){ 
            if(p.getResult().getHgEvaluate().equals(evaluate)){
                i += 1;
            }
        }
        return i;
    }
    
    public static double As(TestSampleDirectory tsd, String evaluate){
        int i = 0;
        for(TestSample p : tsd.getTestSampleDirectory()){ 
            if(p.getResult().getAsEvaluate().equals(evaluate)){
                i += 1;
            }
        }
        return i;
    }
    
    
    public static double Ph(TestSampleDirectory tsd, String evaluate){
        int i = 0;
        for(TestSample p : tsd.getTestSampleDirectory()){ 
            if(p.getResult().getPhEvaluate().equals(evaluate)){
                i += 1;
            }
        }
        return i;
    }
    
    
    public static double Cd(TestSampleDirectory tsd, String evaluate){
        int i = 0;
        for(TestSample p : tsd.getTestSampleDirectory()){ 
            if(p.getResult().getCdEvaluate().equals(evaluate)){
                i += 1;
            }
        }
        return i;
    }
    
    
    public static double Fe(TestSampleDirectory tsd, String evaluate){
        int i = 0;
        for(TestSample p : tsd.getTestSampleDirectory()){ 
            if(p.getResult().getFeEvaluate().equals(evaluate)){
                i += 1;
            }
        }
        return i;
    }
    
    
    public static double Se(TestSampleDirectory tsd, String evaluate){
        int i = 0;
        for(TestSample p : tsd.getTestSampleDirectory()){ 
            if(p.getResult().getSeEvaluate().equals(evaluate)){
                i += 1;
            }
        }
        return i;
    }
    
    
    public static double Dichlorvos(TestSampleDirectory tsd, String evaluate){
        int i = 0;
        for(TestSample p : tsd.getTestSampleDirectory()){ 
            if(p.getResult().getDichlorvosEvaluate().equals(evaluate)){
                i += 1;
            }
        }
        return i;
    }
    
    
    public static double Fenthion(TestSampleDirectory tsd, String evaluate){
        int i = 0;
        for(TestSample p : tsd.getTestSampleDirectory()){ 
            if(p.getResult().getFenthionEvaluate().equals(evaluate)){
                i += 1;
            }
        }
        return i;
    }

    
    public ChartPanel getChartPanel(){  
        return frame1;  

    }  
}
