/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Analysis;

import Business.EcoSystem;
import Business.Network.Network;
import Business.Patient.Patient;
import Business.Patient.PatientDirectory;
import Business.TestSample.TestSample;
import Business.TestSample.TestSampleDirectory;
import java.util.ArrayList;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

/**
 *
 * @author fanshujie
 */
public class QualifiedBarChart {
    ChartPanel frame1;
    
    public QualifiedBarChart(EcoSystem business, TestSampleDirectory tsd, PatientDirectory pd, PatientDirectory pd1){  
        CategoryDataset dataset = getDataSet(business, tsd, pd, pd1);
        
        JFreeChart chart = ChartFactory.createBarChart3D(  
                            "Comparation Between States", // 图表标题  
                            "State", // 目录轴的显示标签  
                            "Ratio", // 数值轴的显示标签  
                            dataset, // 数据集  
                            PlotOrientation.VERTICAL, // 图表方向：水平、垂直  
                            true,           // 是否显示图例(对于简单的柱状图必须是false)  
                            false,          // 是否生成工具  
                            false           // 是否生成URL链接  
                            );  
           
            
         frame1=new ChartPanel(chart,true);        //这里也可以用chartFrame,可以直接生成一个独立的Frame  
           
    }  
    
    private static CategoryDataset getDataSet(EcoSystem business, TestSampleDirectory tsd, PatientDirectory pd, PatientDirectory pd1) {  
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        ArrayList<Network> networks = countNetworks(business, tsd);
        for(Network n : networks){
            dataset.addValue(QualifiedCount(business, tsd, n)/CountSamples(business, tsd, n), "Test Sample Ratio of qualified", n.getName());
            dataset.addValue(PatientRelatedCount(business, pd1, n)/PatientCount(business, pd, n), "Disease Case Related to Food Safety Ratio", n.getName());
        }

        return dataset;  
    }  
    
    
    public static ArrayList<Network> countNetworks(EcoSystem business, TestSampleDirectory tsd){
        ArrayList<Network> netWork = new ArrayList<>();
        for(TestSample ts : tsd.getTestSampleDirectory()){           
            if(!netWork.contains(business.findNetwork(ts.getEnterprise()))){
                netWork.add(business.findNetwork(ts.getEnterprise()));
            }
        }
        return netWork;
    }
    
    public static double QualifiedCount(EcoSystem business, TestSampleDirectory tsd, Network network){
        int i = 0;
        for(TestSample p : tsd.getTestSampleDirectory()){ 
            if(business.findNetwork(p.getEnterprise()) == network){
                if(p.getResult().getEvaluate().equals("Unqualified")){
                    i += 1;
                }
            }
        }
        return i;
    }
    
    
    
    public static double CountSamples(EcoSystem business, TestSampleDirectory tsd, Network network){
        int i = 0;
        for(TestSample p : tsd.getTestSampleDirectory()){ 
            if(business.findNetwork(p.getEnterprise()) == network){
                i += 1;
            }
        }
        return i;
    }
    
    
    public static double PatientRelatedCount(EcoSystem business, PatientDirectory pd1, Network network){
        int i = 0;
        for(Patient p : pd1.getPatientDirectory()){ 
            if(business.findNetworkForPatient(p) == network){
                i += 1;
            }
        }
        return i;
    }
    
    public static double PatientCount(EcoSystem business, PatientDirectory pd, Network network){
        int i = 0;
        for(Patient p : pd.getPatientDirectory()){ 
            if(business.findNetworkForPatient(p) == network){
                i += 1;
            }
        }
        return i;
    }
    
    public ChartPanel getChartPanel(){  
        return frame1;  

    }  
}
