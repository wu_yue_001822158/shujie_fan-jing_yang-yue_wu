/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.Patient.PatientDirectory;

/**
 *
 * @author fanshujie
 */
public class ResearchInstituteWorkRequest extends WorkRequest {
    private PatientDirectory patientDirectory;
    
    public ResearchInstituteWorkRequest(){
        patientDirectory = new PatientDirectory();
    }

    public PatientDirectory getPatientDirectory() {
        return patientDirectory;
    }

    public void setPatientDirectory(PatientDirectory patientDirectory) {
        this.patientDirectory = patientDirectory;
    }
    
    
}
