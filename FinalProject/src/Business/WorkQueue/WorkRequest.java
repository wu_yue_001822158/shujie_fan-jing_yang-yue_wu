/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.Network.Network;
import Business.Result.ResultDirectory;
import Business.UserAccount.UserAccount;

/**
 *
 * @author fanshujie
 */
public class WorkRequest {
    private int id;
    private String message;
    private UserAccount sender;
    private UserAccount receiver;
    private Network receiverNetwork;
    private String status;
    private int linkID;
    private static int counter = 0;

    
    public WorkRequest(){
        counter++;
        id = counter;       
    }
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserAccount getSender() {
        return sender;
    }

    public void setSender(UserAccount sender) {
        this.sender = sender;
    }

    public UserAccount getReceiver() {
        return receiver;
    }

    public void setReceiver(UserAccount receiver) {
        this.receiver = receiver;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLinkID() {
        return linkID;
    }

    public void setLinkID(int linkID) {
        this.linkID = linkID;
    }

    public Network getReceiverNetwork() {
        return receiverNetwork;
    }

    public void setReceiverNetwork(Network receiverNetwork) {
        this.receiverNetwork = receiverNetwork;
    }

    
    
    
    @Override
    public String toString(){
        return String.valueOf(this.id);
    }

}
