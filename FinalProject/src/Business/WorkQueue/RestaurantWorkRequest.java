/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.TestSample.TestSample;

/**
 *
 * @author wuyue
 */
public class RestaurantWorkRequest extends WorkRequest {
    private TestSample testSample;

    public TestSample getTestSample() {
        return testSample;
    }

    public void setTestSample(TestSample testSample) {
        this.testSample = testSample;
    }
    
    
}
