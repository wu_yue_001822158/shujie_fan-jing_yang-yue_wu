/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import java.util.ArrayList;

/**
 *
 * @author fanshujie
 */
public class RIWorkQueue {
     private ArrayList<ResearchInstituteWorkRequest> RIworkRequestList;

    public RIWorkQueue() {
        RIworkRequestList = new ArrayList<>();
    }

    public ArrayList<ResearchInstituteWorkRequest> getRIworkRequestList() {
        return RIworkRequestList;
    }

    public void setRIworkRequestList(ArrayList<ResearchInstituteWorkRequest> RIworkRequestList) {
        this.RIworkRequestList = RIworkRequestList;
    }
    
    public ResearchInstituteWorkRequest searchRequest(int id){
        for(ResearchInstituteWorkRequest wr : RIworkRequestList){
            if(wr.getId() == id){
                return wr;
            }
        }
        return null;
    }
}
