/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import java.util.ArrayList;

/**
 *
 * @author fanshujie
 */
public class WorkQueue {
    private ArrayList<WorkRequest> workRequestList;

    public WorkQueue() {
        workRequestList = new ArrayList<>();
    }

    public ArrayList<WorkRequest> getWorkRequestList() {
        return workRequestList;
    }
    
    public WorkRequest searchRequest(int id){
        for(WorkRequest wr : workRequestList){
            if(wr.getId() == id){
                return wr;
            }
        }
        return null;
    }
    
    public boolean IdIsUnique(int id){
        for(WorkRequest wr : workRequestList){
            if(wr.getId() == id){
                return false;
            }
        }
        return true;
    }
}
