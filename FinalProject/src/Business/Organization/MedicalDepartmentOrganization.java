/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;


import Business.Patient.PatientDirectory;
import Business.Role.Role;
import Business.Role.MedicalDepartmentManagerRole;

import java.util.ArrayList;

/**
 *
 * @author fanshujie
 */
public class MedicalDepartmentOrganization extends Organization {
    private PatientDirectory patientDirectory;
    
    public MedicalDepartmentOrganization() {        
        super(Organization.Type.MedicalDepartment.getValue());
        patientDirectory = new PatientDirectory();
    }

    public PatientDirectory getPatientDirectory() {
        return patientDirectory;
    }

    public void setPatientDirectory(PatientDirectory patientDirectory) {
        this.patientDirectory = patientDirectory;
    }
    
    
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new MedicalDepartmentManagerRole());
        return roles;
    }
}
