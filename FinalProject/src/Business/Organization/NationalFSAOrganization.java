/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;


import Business.Role.NationalFSAManagerRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author fanshujie
 */
public class NationalFSAOrganization extends Organization {
    public NationalFSAOrganization() {
        super(Organization.Type.NationalFSA.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new NationalFSAManagerRole());
        return roles;
    }
}
