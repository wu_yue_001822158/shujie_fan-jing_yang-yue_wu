/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.TestSample;

import Business.Enterprise.Enterprise;
import Business.Result.Result;

/**
 *
 * @author fanshujie
 */
public class TestSample {
    private String name;
    private Enterprise enterprise;
    private Result result;

    public String getName() {
        return name;
        
    }

    public void setName(String name) {
        this.name = name;
    }

    public Enterprise getEnterprise() {
        return enterprise;
    }

    public void setEnterprise(Enterprise enterprise) {
        this.enterprise = enterprise;
    }

    public Result getResult() {
        return result;
    }

    public void setResult(Result result) {
        this.result = result;
    }
    
    
    
    
    
    @Override
    public String toString(){
        return this.name;
    }
}
