/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.TestSample;

import java.util.ArrayList;

/**
 *
 * @author fanshujie
 */
public class TestSampleDirectory {
    private ArrayList<TestSample> testSampleDirectory;
    
    public TestSampleDirectory(){
        testSampleDirectory = new ArrayList<>();
    }

    public ArrayList<TestSample> getTestSampleDirectory() {
        return testSampleDirectory;
    }

    public void setTestSampleDirectory(ArrayList<TestSample> testSampleDirectory) {
        this.testSampleDirectory = testSampleDirectory;
    }
    
    
}
