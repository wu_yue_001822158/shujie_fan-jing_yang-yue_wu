/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;


import Business.Role.Role;
import Business.Role.MedicalDepartmentManagerRole;

import java.util.ArrayList;

/**
 *
 * @author fanshujie
 */
public class MedicalDepartmentOrganization extends Organization {
    public MedicalDepartmentOrganization() {
        super(Organization.Type.MedicalDepartment.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new MedicalDepartmentManagerRole());
        return roles;
    }
}
