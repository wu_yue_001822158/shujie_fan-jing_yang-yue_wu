/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import java.util.ArrayList;

/**
 *
 * @author fanshujie
 */
public class OrganizationDirectory {
    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList<>();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(Organization.Type type){
        Organization organization = null;
        if (type.getValue().equals(Organization.Type.Admin.getValue())){
            organization = new AdminOrganization();
            organizationList.add(organization);
        }
        if (type.getValue().equals(Organization.Type.NationalFSA.getValue())){
            organization = new NationalFSAOrganization();
            organizationList.add(organization);
        }
        if (type.getValue().equals(Organization.Type.StateFSA.getValue())){
            organization = new StateFSAOrganization();
            organizationList.add(organization);
        }
        if (type.getValue().equals(Organization.Type.Lab.getValue())){
            organization = new LabOrganization();
            organizationList.add(organization);
        }
        if (type.getValue().equals(Organization.Type.MedicalDepartment.getValue())){
            organization = new MedicalDepartmentOrganization();
            organizationList.add(organization);
        }
        if (type.getValue().equals(Organization.Type.InternationalResearchInstitute.getValue())){
            organization = new InternationalResearchInstituteOrganization();
            organizationList.add(organization);
        }
        if (type.getValue().equals(Organization.Type.ResearchInstitute.getValue())){
            organization = new ResearchInstituteOrganization();
            organizationList.add(organization);
        }
        if (type.getValue().equals(Organization.Type.Restaurant.getValue())){
            organization = new RestaurantOrganization();
            organizationList.add(organization);
        }
        return organization;
    }
}
